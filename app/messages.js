const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const fileDb = require('../fileDb');
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {
    res.send(fileDb.getItems());
});

router.post('/', upload.single('image'), (req, res) => {
    const message = req.body;
    console.log(req.file);

    if(req.file) {
        message.image = req.file.filename;
    }
    if(req.body.author === '') {
        message.author = 'Anonymous'
    }
    if(!req.body.message) {
        return res.status(400).send({"error": "Message text is required!"});
    } else {
        message.id = nanoid();
        fileDb.addItem(message);
        res.send({message: 'OK'});
    }
});

module.exports = router;